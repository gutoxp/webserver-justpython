#!/usr/bin/env python
"""
Very simple HTTP server in python (Updated for Python 3.7)
"""
import argparse
from http.server import ThreadingHTTPServer
from webserver import BaseHandler


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Executando o 'simple HTTP server'")
    parser.add_argument(
        "-l",
        "--listen",
        default="localhost",
        help="Expecifique o IP que o servidor ira escutar",
    )
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        default=8000,
        help="Expecifique a porta que o servidor ira escutar",
    )
    args = parser.parse_args()

    try:
        httpd = ThreadingHTTPServer((args.listen, args.port), BaseHandler)
        print(f"servidor web rodando no endereço http://{args.listen}:{args.port}/")
        httpd.serve_forever(5)
    except KeyboardInterrupt:
        print("Voce pressionou ^C, encerrando...")
        httpd.socket.close()
