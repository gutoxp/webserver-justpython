#! /bin/bash

# Script para teste de desenpenho da Aplicação
# Primeiro Parameto <numero de requests>
# Segunto Parametro <requests concorents>

ab -n $1 -c $2 -g data_test.tsv  http://localhost:8000/
gnuplot <<EOF

    # Let's output to a jpeg file
    set terminal jpeg size 1024,768
    # This sets the aspect ratio of the graph
    set size 1, 1
    # The file we'll write to
    set output "./graph_request_$1_$2.jpg"
    # The graph title
    set title "Benchmark testing (Requests $1 - Competitor $2)"
    # Where to place the legend/key
    set key left top
    # Draw gridlines oriented on the y axis
    set grid y
    # Label the x-axis
    set xlabel 'requests'
    # Label the y-axis
    set ylabel "response time (ms)"
    # Tell gnuplot to use tabs as the delimiter instead of spaces (default)
    set datafile separator '\t'
    # Plot the data
    plot "./data_test.tsv" every ::2 using 5 title 'Response time' with lines
    exit
EOF
rm data_test.tsv